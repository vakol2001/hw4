﻿using ProjectManager.BL.Entities;
using System.Collections.Generic;

namespace ProjectManager.BL.Models
{
    public record TeamModel
    {
        public int Id { get; init; }
        public string Name { get; init; }
        public IEnumerable<User> Participants { get; init; }
    }
}
