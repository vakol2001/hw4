﻿using ProjectManager.BL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectManager.BL.Interfaces
{
    public interface IConnectService : IDisposable
    {
        System.Threading.Tasks.Task<IEnumerable<Project>> GetProjectsAsync();
        System.Threading.Tasks.Task<Project> GetProjectAsync(int id);
        System.Threading.Tasks.Task<IEnumerable<Task>> GetTasksAsync();
        System.Threading.Tasks.Task<Task> GetTaskAsync(int id);
        System.Threading.Tasks.Task<IEnumerable<User>> GetUsersAsync();
        System.Threading.Tasks.Task<User> GetUserAsync(int id);
        System.Threading.Tasks.Task<IEnumerable<Team>> GetTeamsAsync();
        System.Threading.Tasks.Task<Team> GetTeamAsync(int id);
    }
}
