﻿using System;
using System.Collections.Generic;

namespace ProjectManager.BL.Entities
{
    public record User
    {
        public int Id { get; init; }

        public int? TeamId { get; init; }
        public Team Team { get; init; }

        public string FirstName { get; init; }
        public string LastName { get; init; }
        public string Email { get; init; }
        public DateTime RegisteredAt { get; init; }
        public DateTime BirthDay { get; init; }

        public IEnumerable<Task> Tasks { get; init; }
        public IEnumerable<Project> Projects { get; init; }

    }
}
