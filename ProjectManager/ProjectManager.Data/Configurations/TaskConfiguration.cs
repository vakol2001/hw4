﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectManager.Data.Entities;

namespace ProjectManager.Data.Configurations
{
    class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.Property(x => x.Id) // Id
                   .ValueGeneratedOnAdd()
                   .UseIdentityColumn(seed: 0, increment: 1);

            builder.HasOne(t => t.Project) // ProjectId
                   .WithMany(p => p.Tasks)
                   .HasForeignKey(t => t.ProjectId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(t => t.Performer) // PerformerId
                   .WithMany(u => u.Tasks)
                   .HasForeignKey(t => t.PerformerId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.Property(t => t.Name) // Name
                   .IsRequired()
                   .HasMaxLength(100);


            builder.HasIndex(t => t.Name);
        }
    }
}
