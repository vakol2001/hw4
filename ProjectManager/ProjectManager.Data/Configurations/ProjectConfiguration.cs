﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectManager.Data.Entities;

namespace ProjectManager.Data.Configurations
{
    class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.Property(x => x.Id) // Id
                   .ValueGeneratedOnAdd()
                   .UseIdentityColumn(seed: 0, increment: 1);

            builder.HasOne(p => p.Author) // AuthorId
                   .WithMany(u => u.Projects)
                   .HasForeignKey(p => p.AuthorId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Team) // TeamId
                   .WithMany(t => t.Projects)
                   .HasForeignKey(p => p.TeamId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Property(p => p.Name) // Name
                   .IsRequired()
                   .HasMaxLength(50);


            builder.HasIndex(p => p.Name);
        }
    }
}
