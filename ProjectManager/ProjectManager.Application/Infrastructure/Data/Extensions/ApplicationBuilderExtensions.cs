﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ProjectManager.Application.Infrastructure.Data.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder SeedData(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetService<ProjectManagerContext>();

            context.Database.EnsureCreated();

            if (!context.Teams.Any())
            {
                var path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Infrastructure\Data\Json\teams.json";
                var entities = JsonConvert.DeserializeObject<IEnumerable<Team>>(File.ReadAllText(path));

                context.Teams.AddRange(entities);
                context.SaveChanges();
            }

            if (!context.Users.Any())
            {
                var path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Infrastructure\Data\Json\users.json";
                var entities = JsonConvert.DeserializeObject<IEnumerable<User>>(File.ReadAllText(path));

                context.Users.AddRange(entities);
                context.SaveChanges();
            }

            if (!context.Projects.Any())
            {
                var path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Infrastructure\Data\Json\projects.json";
                var entities = JsonConvert.DeserializeObject<IEnumerable<Project>>(File.ReadAllText(path));

                context.Projects.AddRange(entities);
                context.SaveChanges();
            }

            if (!context.Tasks.Any())
            {
                var path = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Infrastructure\Data\Json\tasks.json";
                var entities = JsonConvert.DeserializeObject<IEnumerable<Task>>(File.ReadAllText(path));

                context.Tasks.AddRange(entities);
                context.SaveChanges();
            }
            return app;
        }


    }
}
