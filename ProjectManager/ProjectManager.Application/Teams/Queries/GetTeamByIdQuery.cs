﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Queries
{
    public class GetTeamByIdQuery : IRequest<TeamModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetTeamByIdQuery, TeamModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TeamModel> Handle(GetTeamByIdQuery request, CancellationToken cancellationToken)
            {
                var team = await _context.Teams.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                var mappedTeam = _mapper.Map<TeamModel>(team);
                return mappedTeam;
            }
        }
    }
}
