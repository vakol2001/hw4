﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Teams.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Teams.Commands
{
    public class UpdateTeamCommand : IRequest<TeamModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public class Handler : IRequestHandler<UpdateTeamCommand, TeamModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TeamModel> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
            {
                var team = await _context.Teams.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                _context.Entry(team).CurrentValues.SetValues(request);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<TeamModel>(team);
            }
        }
    }
}
