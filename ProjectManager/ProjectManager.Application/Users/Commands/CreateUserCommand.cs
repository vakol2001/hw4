﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Users.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Users.Commands
{
    public class CreateUserCommand : IRequest<UserModel>
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }


        public class Handler : IRequestHandler<CreateUserCommand, UserModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<UserModel> Handle(CreateUserCommand request, CancellationToken cancellationToken)
            {
                var userToAdd = _mapper.Map<User>(request);
                var user = await _context.Users.AddAsync(userToAdd, cancellationToken);
                var mappedUser = _mapper.Map<UserModel>(user.Entity);
                await _context.SaveChangesAsync(cancellationToken);
                return mappedUser;
            }
        }
    }
}
