﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Queries
{
    public class GetAllTasksQuery : IRequest<IEnumerable<ProjectModel>>
    {
        public class Handler : IRequestHandler<GetAllTasksQuery, IEnumerable<ProjectModel>>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<IEnumerable<ProjectModel>> Handle(GetAllTasksQuery request, CancellationToken cancellationToken)
            {
                var projects = await _context.Projects.ToListAsync(cancellationToken);
                var mappedProjects = _mapper.Map<IEnumerable<ProjectModel>>(projects);
                return mappedProjects;
            }
        }
    }
}
