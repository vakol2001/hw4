﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Commands
{
    public class UpdateProjectCommand : IRequest<ProjectModel>
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public class Handler : IRequestHandler<UpdateProjectCommand, ProjectModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<ProjectModel> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
            {
                var project = await _context.Projects.FindAsync(new object[] { request.Id }, cancellationToken) 
                    ?? throw new KeyNotFoundException();

                _context.Entry(project).CurrentValues.SetValues(request);
                await _context.SaveChangesAsync(cancellationToken);
                return _mapper.Map<ProjectModel>(project);
            }
        }
    }
}
