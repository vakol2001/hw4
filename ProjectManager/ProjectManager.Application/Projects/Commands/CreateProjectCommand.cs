﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Projects.Models;
using ProjectManager.Data;
using ProjectManager.Data.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Projects.Commands
{
    public class CreateProjectCommand : IRequest<ProjectModel>
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public class Handler : IRequestHandler<CreateProjectCommand, ProjectModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<ProjectModel> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
            {
                var projectToAdd = _mapper.Map<Project>(request);
                var project = await _context.Projects.AddAsync(projectToAdd, cancellationToken);
                var mappedProject = _mapper.Map<ProjectModel>(project.Entity);
                await _context.SaveChangesAsync(cancellationToken);
                return mappedProject;
            }
        }
    }
}
