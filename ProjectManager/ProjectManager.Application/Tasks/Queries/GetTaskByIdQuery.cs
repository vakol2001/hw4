﻿using AutoMapper;
using MediatR;
using ProjectManager.Application.Tasks.Models;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Tasks.Queries
{
    public class GetTaskByIdQuery : IRequest<TaskModel>
    {
        public int Id { get; set; }


        public class Handler : IRequestHandler<GetTaskByIdQuery, TaskModel>
        {
            private readonly ProjectManagerContext _context;
            private readonly IMapper _mapper;

            public Handler(IMapper mapper, ProjectManagerContext context)
            {
                _context = context;
                _mapper = mapper;
            }


            public async Task<TaskModel> Handle(GetTaskByIdQuery request, CancellationToken cancellationToken)
            {
                var task = await _context.Tasks.FindAsync(new object[] { request.Id }, cancellationToken)
                    ?? throw new KeyNotFoundException();

                var mappedTask = _mapper.Map<TaskModel>(task);
                return mappedTask;
            }
        }
    }
}
