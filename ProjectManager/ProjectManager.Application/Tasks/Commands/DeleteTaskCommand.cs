﻿using MediatR;
using ProjectManager.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectManager.Application.Tasks.Commands
{
    public class DeleteTaskCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteTaskCommand>
        {
            private readonly ProjectManagerContext _context;

            public Handler(ProjectManagerContext context)
            {
                _context = context;
            }


            public async Task<Unit> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
            {
                var task = await _context.Tasks.FindAsync(new object[] { request.Id }, cancellationToken) ?? throw new KeyNotFoundException();
                _context.Tasks.Remove(task);
                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }

        }
    }
}
